import { Http , Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';





@Injectable()
export class DataProvider {


	server:any;
	club_id:any;
  datoeliminar:any={};
  datajugador:any={};
  data:any={}
  xiencias:any;

  constructor(private toastCtrl: ToastController,public http: HttpClient,public storage: Storage) {
    console.log('Hello DataProvider Provider');

    this.server = "http://comunica7.com/apirestsmartgoal/public/";

    this.xiencias= "http://xiencias.com:9000/"

       this.storage.get('usuario').then((val) => {

         if(val){

             this.club_id=val.club

         }
 			
    
        })

    
    
  }



  presentToast(data) {
  let toast = this.toastCtrl.create({
    message: data,
    duration: 3000,
    position: 'bottom'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
}





   traeclubs(club) {
      return this.http.get(this.server+'list/club/null/'+club)
      .map((response: Response) => response)

   }


   listajuegos(club){



   		return this.http.get(this.xiencias+'juegos/'+club)
      .map((response: Response) => response)


   }


   traejugadores(club,data){


       

       return this.http.get(this.server+'list/jugadores/1/'+club+'/'+data)
      .map((response: Response) => response)


   }



   items(juego,club){


       ///list/juegodet/{fjuego}/{fclub}/{fempresa}'

       return this.http.get(this.xiencias+'items/'+club+'/'+juego)
      .map((response: Response) => response)


   }


   guardajuego(club,adddescripcion){


     console.log('Guarda Juego...')

      /*data.addclub=1
      data.addempresa=1
      data.addusuario=1
      data.addproceso=1

      return this.http.post(this.server+'add/juego/',data)
      .map((response: Response) => response)*/


      this.data.addclub=club
      this.data.addempresa=1
      this.data.addusuario=1
      this.data.addproceso=1
      this.data.adddescripcion=adddescripcion



     this.http.post('http://comunica7.com/apirestsmartgoal/public/add/juego',this.data)
    .subscribe(
      data => {


        console.log(data)

         this.presentToast('Seleccione una area')
         


        },

      error =>{



      })







   }


  ingresar(data){


      console.log('ngresando...',data)
       return this.http.get(this.server+'login/validar/'+data.usuario+'/'+data.password+'/1/'+data.club)
      .map((response: Response) => response)


  }


  ///list/juegodetjugador {fjuego}/{fclub}/{fempresa}/{fitem}


  listajuegodetallejugador(juego,club,item){


       return this.http.get(this.xiencias+'detallejugadores/'+club+'/'+juego+'/'+item)
      .map((response: Response) => response)


  }


 

  //'/list/tbgenerald/{codpadre}'


  // Tipo de Rol

    listatipoderol(){


       return this.http.get(this.server+'list/tbgenerald/01')
      .map((response: Response) => response)


  }

   listatipodedocumento(){


       return this.http.get(this.server+'list/tbgenerald/02')
      .map((response: Response) => response)


  }

  listasexos(){

           return this.http.get(this.server+'list/tbgenerald/03')
      .map((response: Response) => response)

  }

    listaarcos(){

           return this.http.get(this.server+'list/tbgenerald/04')
      .map((response: Response) => response)

  }


      listaposicion(){

           return this.http.get(this.server+'list/tbgenerald/06')
      .map((response: Response) => response)

  }

     listapies(){

           return this.http.get(this.server+'list/tbgenerald/06')
      .map((response: Response) => response)

  }


   guardatiposecuencia(juego,secuencia){

           return this.http.get(this.xiencias+'guardatiposecuencia/'+juego+'/'+secuencia)
      .map((response: Response) => response)

  }


  listatiros(){

           return this.http.get(this.server+'list/tbgenerald/05')
      .map((response: Response) => response)

  }


  sacajugador(codigo,club){


      console.log(codigo,club)

      
         return this.http.get(this.server+'query/jugadores/'+codigo+'/1/'+club)
      .map((response: Response) => response)


  }


  eliminajuego(codigo,club){


    console.log('Eliminando..')

    

    this.datoeliminar.dproceso= 3
    this.datoeliminar.dusuario= 1
    this.datoeliminar.ddescripcion = 1

    //'/delete/juego/{dcodigo}/{dclub}/{dempresa}' :: ddescripcion,dusuario,dproceso

         return this.http.post(this.server+'delete/juego/'+codigo+'/'+club+'/1',this.datoeliminar).
         map((response: Response) =>  response)



  }

   guardapuntaje(codigo,club,jugador,item,puntaje,area){

   if(item==undefined){item=1}
   if(codigo==undefined){codigo=1}
   this.datajugador.item=item
   this.datajugador.juego=  codigo
   this.datajugador.club =  club
   this.datajugador.jugador = jugador
   this.datajugador.puntaje = puntaje
   this.datajugador.area = area

   console.log('Enviando puntaje...',this.datajugador)


 this.http.post(this.xiencias+'guardapuntaje',this.datajugador)
    .subscribe(
      data => {

          
          this.presentToast('Puntaje Guardado')


        },

      error =>{




      })



 }

  // http://comunica7.com/apirestsmartgoal/public/query/juegojugadorcuadro/1/1/1/1/1/2019-01-02/2019-06-20
 listajugadas(club,jugador,tipotiro,juego,finicio,ffin){



//return this.http.get(this.server+'query/juegojugadorcuadro/1/'+club+'/'+jugador+'/'+tipotiro+'/'+juego+'/'+finicio+'/'+ffin)
     // .map((response: Response) => response)


     return this.http.get(this.xiencias+'historial/'+club)
      .map((response: Response) => response)



 }







}
