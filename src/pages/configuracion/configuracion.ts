import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FiltroPage } from '../filtro/filtro';
import { Storage } from '@ionic/storage';
import { DataProvider } from '../../providers/data/data';


/**
 * Generated class for the ConfiguracionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuracion',
  templateUrl: 'configuracion.html',
})
export class ConfiguracionPage {


  homePage:any;
  historial:any;
  juegos:any;
  club_id:any;
  jugadores:any;

  club:any;

  clubes:any;

  constructor(public dataprovider:DataProvider,public storage:Storage,public modalCtrl: ModalController,private view:ViewController,public navCtrl: NavController, public navParams: NavParams) {

    this.homePage=HomePage
    this.jugadores=true

       this.storage.get('usuario').then((val) => {

         if(val){

             this.club_id=val.club

             dataprovider.traeclubs("null").subscribe(data=>{


                 this.clubes = data

               
                 this.club =this.clubes.filter(data => data.Codigo == this.club_id)[0];

                 console.log(this.club)


             })

         }
       
    
        })


         



  }







  ionViewDidLoad() {

    console.log('ionViewDidLoad ConfiguracionPage');
  

  }


  closeModal(){

    this.view.dismiss()

	}

  
  juego(){


  }

  salir(){

    console.log('data')

    this.storage.remove('usuario')


  }

  filtro(){

     let profileModal = this.modalCtrl.create(FiltroPage, {});
   profileModal.onDidDismiss(data => {



       console.log(data)

       

     

   });
   profileModal.present();

  }

}
