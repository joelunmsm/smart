import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,AlertController,Platform, ViewController } from 'ionic-angular';
import { AgregadetallejuegoPage } from '../agregadetallejuego/agregadetallejuego';
import { Http,RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { Storage } from '@ionic/storage';
/**
/**
 * Generated class for the DetallejuegoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detallejuego',
  templateUrl: 'detallejuego.html',
})
export class DetallejuegoPage {


	juego:any
	detallejuegos:any=[]
  club_id:any;

	//usp_lis_juego_det '/list/juegodet/{fjuego}/{fclub}/{fempresa}'

  constructor(public storage:Storage,public dataprovider: DataProvider,private http: HttpClient,public navCtrl: NavController, public navParams: NavParams,public modalCtrl:ModalController,public view: ViewController) {

  	this.juego= navParams.get("juego")


        this.storage.get('usuario').then((val) => {

         if(val){

             this.club_id=val.club

               this.listadetallesjuego(this.club_id)

         }
       
    
        })



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetallejuegoPage');

  

  
        
  }




    closeModal(){


  
    

    this.view.dismiss()
}


listadetallesjuego(club){




		 this.http.get('http://comunica7.com/apirestsmartgoal/public/list/juegodet/'+this.juego.Codigo+'/'+club+'/1')
    .subscribe(
      data => {

          console.log('listadetallesjuego',data)


         
          this.detallejuegos=data
   
          //this.clubes =data


        },

      error =>{



      })





}

agregadetallejuego(){

   


    this.navCtrl.push(HomePage,{juego:this.juego}).then(() => {
  this.navCtrl.getActive().onDidDismiss(data => {

      this.listadetallesjuego(this.club_id)
    
  });
});

}

editardetalle(data){

     this.navCtrl.push(HomePage,{editajuego:data,juego:this.juego}).then(() => {
  this.navCtrl.getActive().onDidDismiss(data => {

      this.listadetallesjuego(this.club_id)
    
  });
});


}


eliminardetalle(data){


  data.djuego=this.juego.Codigo
  data.dempresa=1
  data.dusuario=1

  //usp_del_juego_det '/delete/juegodet/{dclub}/{ditem}' :: djuego,dempresa,dusuario

  console.log(data)


   this.http.put('http://comunica7.com/apirestsmartgoal/public/delete/juegodet/'+this.club_id+'/'+data.item,data)
    .subscribe(
      data => {

          console.log('elimi',data)
          this.listadetallesjuego(this.club_id)

        },

      error =>{



      })

}



}
