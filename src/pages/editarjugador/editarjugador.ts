import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Http,RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HomePage } from '../home/home';
import { DataProvider } from '../../providers/data/data';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the EditarjugadorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editarjugador',
  templateUrl: 'editarjugador.html',
})
export class EditarjugadorPage {


	jugador:any;
	editajugador:any={}
	club_id:any;
	sexos:any;
	tiposdedoc:any;


  constructor(public view:ViewController,public storage:Storage,public dataprovider:DataProvider,private http: HttpClient,public navCtrl: NavController, public navParams: NavParams) {



  	this.jugador = navParams.get("jugador")


  	 this.sexos = dataprovider.listasexos()

     this.tiposdedoc = dataprovider.listatipodedocumento()



   this.storage.get('usuario').then((val) => {

         if(val){

             this.club_id=val.club


               	dataprovider.sacajugador(this.jugador.Codigo,this.club_id).subscribe(data=>{



               				console.log('trayendo jugador..',data)

               				let datajugador = data[0]

							this.jugador.updnombres =datajugador.Nombres
							this.jugador.updapellidos= datajugador.Apellidos
							this.jugador.updtipodoc =datajugador.Tipo_doc
							this.jugador.updnrodoc =datajugador.Nrodoc
							this.jugador.updsexo =datajugador.Sexo
							this.jugador.updpeso =datajugador.Peso
							this.jugador.updtalla =datajugador.Talla
							this.jugador.updposicion= datajugador.Posicion
							this.jugador.updpies =datajugador.Pies
							this.jugador.updemail =datajugador.email
							this.jugador.updtelefono= datajugador.Telefono

			  	})

            

         }
       
    
        })








  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditarjugadorPage');
  }


   closeModal(){

  	this.view.dismiss()
  }



  actualiza(jugador){

	jugador.updproceso = 2
	jugador.updusuario = 1


console.log('Actualizando...')


this.http.put('http://comunica7.com/apirestsmartgoal/public/update/jugadores/'+jugador.Codigo+'/1/'+this.club_id,jugador)
    .subscribe(
      data => {

          console.log('ooo',data)


          this.closeModal()

   
          //this.clubes =data


        },

      error =>{



      })






  }



 

}
