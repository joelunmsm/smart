import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';

/**
 * Generated class for the CalificajugadorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calificajugador',
  templateUrl: 'calificajugador.html',
})
export class CalificajugadorPage {


	  uno:any=false;
  dos:any=false;
  tres:any=false;
  cuatro:any=false;
  cinco:any=false;
  jugador:any;
  nota:any;
  calificacion:any;

  constructor(private view:ViewController,public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams) {
    
    this.jugador = navParams.get("jugador");

    this.calificacion = navParams.get("calificacion");


      console.log('calificacion.....',this.calificacion)

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalificajugadorPage');
  }


  



   califica(data){

  	this.nota=data

  	this.agregatoast('Perfecto, calificaste con '+data+ ' estrellas')

  	console.log(data)

  	if(data==1){
  		this.uno=true
  	}

  	if(data==2){
  		this.uno=true
  		this.dos=true
  	}

  	if(data==3){
  		this.uno=true
  		this.dos=true
  		this.tres=true
  	}

  	 if(data==4){
  		this.uno=true
  		this.dos=true
  		this.tres=true
  		this.cuatro=true
  	}

  	if(data==5){
  		this.uno=true
  		this.dos=true
  		this.tres=true
  		this.cuatro=true
  		this.cinco=true
  	}


  }


   agregatoast(data) {
    let toast = this.toastCtrl.create({
      message: data,
      duration: 2000,
      cssClass: 'mitoast',
      position:'bottom',
    });
    toast.present();
  }

  descalifica(data){

  	  	this.agregatoast('Perfecto, calificaste con '+data+ ' estrellas')

  	this.nota=data

  	if(data==1){
  		this.uno=true
  		 this.dos=false
  		this.tres=false
  		this.cuatro=false
  		this.cinco=false
  	}

  	if(data==2){
  		this.uno=true
  		this.dos=true
  		this.tres=false
  		this.cuatro=false
  		this.cinco=false
  	}

  	if(data==3){
  		this.uno=true
  		this.dos=true
  		this.tres=true
  		this.cuatro=false
  		this.cinco=false
  	}

  	if(data==4){
  		this.uno=true
  		this.dos=true
  		this.tres=true
  		this.cuatro=true
  		this.cinco=false
  	}

  	if(data==5){
    	this.uno=true
  		this.dos=true
  		this.tres=true
  		this.cuatro=true
  		this.cinco=true
  	}

  }


  closeModal(){


   
    this.view.dismiss()


}


}
