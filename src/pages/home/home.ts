import { Component,ViewChild, Renderer } from '@angular/core';
import { NavController,ToastController,AlertController,ViewController,ModalController, NavParams,LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Events, Platform } from 'ionic-angular';
import { ConfiguracionPage } from '../configuracion/configuracion';
import { CalificajugadorPage } from '../calificajugador/calificajugador';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Http,RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { DataProvider } from '../../providers/data/data';
import { SeleccionajugadorComponent } from '../../components/seleccionajugador/seleccionajugador';
import { ResultadoComponent } from '../../components/resultado/resultado';
import { MiperfilComponent } from '../../components/miperfil/miperfil';
import { TableroComponent } from '../../components/tablero/tablero';
import { JuegosComponent } from '../../components/juegos/juegos';
import { PuntajesComponent } from '../../components/puntajes/puntajes';
import { ConsecutivoComponent } from '../../components/consecutivo/consecutivo';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('myCanvas') canvas: any;


    canvasElement: any;
    lastX: number;
    lastY: number;
    letra:any;
    _juegos:any;
    configuracionPage:any;

    currentColour: string = '#1abc9c';
    availableColours: any;
    tiposecuencia:any;

    brushSize: number = 2;
    Xi:any;
    Yi:any;
    
  menujuego:any=false;
  _jugadores:any;
  id_juego:any;

  nombre_juego:any;
  muestraplay:any=false;
  selec:any;
  numerorepetir:any=0;
  index_numerorepetir=0
  estado:any;
  colormemoria:any='light'
  listajugadores:any;
  p:any;
  activoarea:any=false;
  grabando:any=false;
  array:any;
  dos_columna:any;
  tres_columna:any;
  cuatro_columna:any;
  primera_fila:any;
  jugador:any;
  now:any;
  primer_reloj:any;
  segundo_reloj:any;
  tercer_reloj:any;
  duracion:any;
  duracion_segundo_reloj:any;
  duracion_tercer_reloj:any;
  encalificacion:any;
  juegos:any={}
  ultimojuego:any;
  muestrapuntaje:any;
  calificacion:any;

  jugadoresdefensa:any;

  juegoguardado:any;

  datajugador:any={}

  jugador_actual:any;
  estado_jugador:any;
  coordenadas:any;
  coordenadas2:any;
  jugadoresseleccionados:any=[];
  tiposdetiroseleccionados:any;
  timerid:any;
  fp:any;
  tipodetiro:any=[{'nombre':'Saques de Esquina'},{'nombre':'Chilena'},{'nombre':'Volea'},{'nombre':'Tijera'},{'nombre':'Tiro Cruzado'}];

  

  alto_pastilla:any;
  indexcalificarandom:any=0;
  indice_item:any=0;

  activaverde:any=false
  activaamarillo:any=false
  activaazul:any=false
  activarojo:any=false

  ancho:any;
  height:any;
  h:any
  columna_izquierda:any;
  columna_derecha:any;
  ancho_arco:any;
  altura_pastilla:any;
  activasecuencia:any=false;
  activarandom:any=false;
  activatiro:any=false;
  activaintermitente:any=false;
  activaplay:any=false
  activaarea:any=false
  clock:any;
  enejecucion:any;
  duracion_primer_reloj:any;
  pausajuego:any=false;

  tiro_raso:any;
  margin_alto:any;
  tipofuncion:any
  indexjugador:any=0;
  xin:any;
  yin:any;
  xfin:any;
  yfin:any;
  item_extras:any;
  indice_item_extras:any=0;

  ordernsecuencia:any;
  codigotiroseleccionado:any;
 
  tiempo:any={}
  arrayjugadores:any=[{'nombre':'Cuevita'},
      {'nombre':'Guerrero'},
      {'nombre':'Farfan'},
      {'nombre':'Tapia'},
      {'nombre':'Galese'},
      {'nombre':'Yotun'}]



  rootPage:any
  array_vacio:any;


  alto_arco:any;
  cabecera:any;
  activapelota:any=false;
  array_secuencia:any=[]
  indexsecuencia:any=0

  secuencia_total:any=[{'nombre':'Primera Secuencia','data':[]},
  {'nombre':'Segunda Secuencia','data':[]},
  {'nombre':'Tercera Secuencia','data':[]},
  {'nombre':'Cuarta Secuencia','data':[]}]


  primera_secuencia:any=[]
  segunda_secuencia:any=[]
  indexsecuenciacalificacion:any=0;


  listasecuencia:any;
  loading:any;
  secuencia_random:any;
  detallejuego:any={};
  club_id:any;
  poste:any;
  areaevaluada:any;
  _tiros:any;
  tiposdetiroseleccionadonombre:any;
  juego:any;


  array_tiro_raso: any =['E7','F7','G7','H7','I7','J7','K7','L7','M7','N7','NN7','O7','P7','Q7','R7']

  array_primer_arco :any = ['A1','A2','A3','A4','A5','A6','A7','A8','B1','C1','D1','E1','F1','G1','H1','I1','J1','K1','L1','M1','N1','NN1','O1','P1','Q1','R1','S1','T1','U1','V1','V2','V3','V4','V5','V6','V7','V8']

  array_segundo_arco :any = ['B2','B3','B4','B5','B6','B7','B8','C2','D2','E2','F2','G2','H2','I2','J2','K2','M2','N2','NN2','O2','P2','Q2','R2','S2','T2','U2','U3','U4','U5','U6','U7','U8']

  array_tercer_arco :any = ['C3','C4','C5','C6','C7','C8','D3','E3','F3','G3','H3','I3','J3','K3','M3','N3','NN3','O3','P3','Q3','S3','T3','T4','T5','T6','T7','T8']

  array_cuarto_arco :any = ['D4','D5','D6','D7','D8','E4','F4','G4','H4','I4','J4','K4','M4','N4','NN4','O4','P4','Q4','R4','S4','S5','S6','S7','S8']

  items:any={};


  editajuego:any;

  constructor(public dataprovider:DataProvider,private http: HttpClient,public loadingCtrl: LoadingController,public navParams:NavParams,private view:ViewController,public modalCtrl: ModalController,private alertCtrl: AlertController,public platform: Platform, public toastCtrl: ToastController,public storage: Storage,public renderer: Renderer,public navCtrl: NavController,public events: Events,private screenOrientation: ScreenOrientation) {

    //Obtiene el juego guardado

    this.juegoguardado = navParams.get("play")


    console.log('juego guardado...',this.juegoguardado)

    // Lista tipos de tiro

    dataprovider.listatiros().subscribe(data=>{ this.tipodetiro=data })


    this.storage.get('usuario').then((val) => {

         if(val){

             this.club_id=val.club

             /// Trae Juego Guardado

             if(this.juegoguardado){

                     //Trae items del juego guardado

                    dataprovider.items(this.juegoguardado,this.club_id).subscribe(data=>{


                        console.log('items.....',data)

                        this.items=data

                        this.item_extras=data

                        if(data[0]['jde_funcion']=='Secuencia'){



                                console.log('Entre a secuencia........................')


                                this.indexsecuencia=this.items.length


                                this.tiposecuencia=data[0]['jde_secuencia']


                               this.armalistasecuencia()


                               for(let i in this.items){

                                   this.storage.set('secuencia'+i,JSON.parse(this.items[i]['jde_area']))

                               }


                               this.activasecuencia=true

                               this.muestraplay=true


                        }

                        if(data[0]['jde_funcion']=='Area'){

                            if(this.items.length>0){

                              console.log('Entre a area........................')
                                
                              this.activoarea=true
                             
                            }

                        }

                        this.seteaitem(this.items[this.indice_item])


                    })


             }

               //Lsita Jugadores

              dataprovider.traejugadores(this.club_id,"null").subscribe(data=>{ this.arrayjugadores=data })

           

         }
       
    
        })


    //Dimensiones de pastillas, arcos

    this.rootPage = ConfiguracionPage;
 
    this.ancho=screen.width

    //this.ancho=1000

    this.poste=0

    this.columna_izquierda=parseInt(this.ancho)*9/100+this.poste

    this.columna_derecha=parseInt(this.ancho)*9/100

    this.height=screen.height

     //header en el html

    this.height=443

    this.margin_alto = 5*this.height/100+this.poste

    if(this.juego){

        this.margin_alto+40
    }

    this.alto_arco=50*this.height/100+this.poste

    this.ancho_arco =this.ancho-this.columna_izquierda-this.columna_derecha-2*this.poste

    let a = parseInt(this.ancho_arco)/23

    let h = parseInt(this.alto_arco)/8

    this.alto_pastilla=h

    console.log('Alto de la pastilla :',h)



    this.array = [

    //PRIMERA FILA A

{'i':1,'pastilla':'A1','A':false,'xin':this.columna_izquierda,'yin':h+this.margin_alto,'xfin':a,'yfin':h,'x':1,'y':1},
{'i':2,'pastilla':'A2','A':false,'xin':this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':a,'yfin':2*h,'x':1,'y':2},
{'i':3,'pastilla':'A3','A':false,'xin':this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':a,'yfin':3*h,'x':1,'y':3},
{'i':4,'pastilla':'A4','A':false,'xin':this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':a,'yfin':4*h,'x':1,'y':4},
{'i':5,'pastilla':'A5','A':false,'xin':this.columna_izquierda,'yin':5*h+this.margin_alto,'xfin':a,'yfin':5*h,'x':1,'y':5},
{'i':6,'pastilla':'A6','A':false,'xin':this.columna_izquierda,'yin':6*h+this.margin_alto,'xfin':a,'yfin':6*h,'x':1,'y':6},
{'i':7,'pastilla':'A7','A':false,'xin':this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':a,'yfin':7*h,'x':1,'y':7},
{'i':8,'pastilla':'A8','A':false,'xin':this.columna_izquierda,'yin':8*h+this.margin_alto,'xfin':a,'yfin':8*h,'x':1,'y':8},

 //SEGUNDA  FILA B
{'i':9,'pastilla':'B1','A':false,'xin':a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':2*a,'yfin':h,'x':2,'y':1},
{'i':10,'pastilla':'B2','A':false,'xin':a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':2*a,'yfin':2*h,'x':2,'y':2},
{'i':11,'pastilla':'B3','A':false,'xin':a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':2*a,'yfin':3*h,'x':2,'y':3},
{'i':12,'pastilla':'B4','A':false,'xin':a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':2*a,'yfin':4*h,'x':2,'y':4},
{'i':13,'pastilla':'B5','A':false,'xin':a+this.columna_izquierda,'yin':5*h+this.margin_alto,'xfin':2*a,'yfin':5*h,'x':2,'y':5},
{'i':14,'pastilla':'B6','A':false,'xin':a+this.columna_izquierda,'yin':6*h+this.margin_alto,'xfin':2*a,'yfin':6*h,'x':2,'y':6},
{'i':15,'pastilla':'B7','A':false,'xin':a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':2*a,'yfin':7*h,'x':2,'y':7},
{'i':16,'pastilla':'B8','A':false,'xin':a+this.columna_izquierda,'yin':8*h+this.margin_alto,'xfin':2*a,'yfin':8*h,'x':2,'y':8},

 //TERCERA FILA C
{'i':17,'pastilla':'C1','A':false,'xin':2*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':3*a,'yfin':h,'x':3,'y':1},
{'i':18,'pastilla':'C2','A':false,'xin':2*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':3*a,'yfin':2*h,'x':3,'y':2},
{'i':19,'pastilla':'C3','A':false,'xin':2*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':3*a,'yfin':3*h,'x':3,'y':3},
{'i':20,'pastilla':'C4','A':false,'xin':2*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':3*a,'yfin':4*h,'x':3,'y':4},
{'i':21,'pastilla':'C5','A':false,'xin':2*a+this.columna_izquierda,'yin':5*h+this.margin_alto,'xfin':3*a,'yfin':5*h,'x':3,'y':5},
{'i':22,'pastilla':'C6','A':false,'xin':2*a+this.columna_izquierda,'yin':6*h+this.margin_alto,'xfin':3*a,'yfin':6*h,'x':3,'y':6},
{'i':23,'pastilla':'C7','A':false,'xin':2*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':3*a,'yfin':7*h,'x':3,'y':7},
{'i':24,'pastilla':'C8','A':false,'xin':2*a+this.columna_izquierda,'yin':8*h+this.margin_alto,'xfin':3*a,'yfin':8*h,'x':3,'y':8},


 //CUARTA FILA D
{'i':25,'pastilla':'D1','A':false,'xin':3*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':4*a,'yfin':h,'x':4,'y':1},
{'i':26,'pastilla':'D2','A':false,'xin':3*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':4*a,'yfin':2*h,'x':4,'y':2},
{'i':27,'pastilla':'D3','A':false,'xin':3*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':4*a,'yfin':3*h,'x':4,'y':3},
{'i':28,'pastilla':'D4','A':false,'xin':3*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':4*a,'yfin':4*h,'x':4,'y':4},
{'i':29,'pastilla':'D5','A':false,'xin':3*a+this.columna_izquierda,'yin':5*h+this.margin_alto,'xfin':4*a,'yfin':5*h,'x':4,'y':5},
{'i':30,'pastilla':'D6','A':false,'xin':3*a+this.columna_izquierda,'yin':6*h+this.margin_alto,'xfin':4*a,'yfin':6*h,'x':4,'y':6},
{'i':31,'pastilla':'D7','A':false,'xin':3*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':4*a,'yfin':7*h,'x':4,'y':7},
{'i':32,'pastilla':'D8','A':false,'xin':3*a+this.columna_izquierda,'yin':8*h+this.margin_alto,'xfin':4*a,'yfin':8*h,'x':4,'y':8},

//  quinta filaa... E.....

{'i':33,'pastilla':'E1','A':false,'xin':4*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':5*a,'yfin':h,'x':5,'y':1},
{'i':34,'pastilla':'E2','A':false,'xin':4*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':5*a,'yfin':2*h,'x':5,'y':2},
{'i':35,'pastilla':'E3','A':false,'xin':4*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':5*a,'yfin':3*h,'x':5,'y':3},
{'i':36,'pastilla':'E4','A':false,'xin':4*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':5*a,'yfin':4*h,'x':5,'y':4},


//quinta filaa... f.....


{'i':37,'pastilla':'F1','A':false,'xin':5*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':6*a,'yfin':h,'x':6,'y':1},
{'i':38,'pastilla':'F2','A':false,'xin':5*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':6*a,'yfin':2*h,'x':6,'y':2},
{'i':39,'pastilla':'F3','A':false,'xin':5*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':6*a,'yfin':3*h,'x':6,'y':3},
{'i':40,'pastilla':'F4','A':false,'xin':5*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':6*a,'yfin':4*h,'x':6,'y':4},




//quinta filaa... g.....



{'i':41,'pastilla':'G1','A':false,'xin':6*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':7*a,'yfin':h,'x':7,'y':1},
{'i':42,'pastilla':'G2','A':false,'xin':6*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':7*a,'yfin':2*h,'x':7,'y':2},
{'i':43,'pastilla':'G3','A':false,'xin':6*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':7*a,'yfin':3*h,'x':7,'y':3},
{'i':44,'pastilla':'G4','A':false,'xin':6*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':7*a,'yfin':4*h,'x':7,'y':4},


//quinta filaa... h.....


{'i':45,'pastilla':'H1','A':false,'xin':7*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':8*a,'yfin':h,'x':8,'y':1},
{'i':46,'pastilla':'H2','A':false,'xin':7*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':8*a,'yfin':2*h,'x':8,'y':2},
{'i':47,'pastilla':'H3','A':false,'xin':7*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':8*a,'yfin':3*h,'x':8,'y':3},
{'i':48,'pastilla':'H4','A':false,'xin':7*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':8*a,'yfin':4*h,'x':8,'y':4},


//quinta filaa... I.....




{'i':49,'pastilla':'I1','A':false,'xin':8*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':9*a,'yfin':h,'x':9,'y':1},
{'i':50,'pastilla':'I2','A':false,'xin':8*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':9*a,'yfin':2*h,'x':9,'y':2},
{'i':51,'pastilla':'I3','A':false,'xin':8*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':9*a,'yfin':3*h,'x':9,'y':3},
{'i':52,'pastilla':'I4','A':false,'xin':8*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':9*a,'yfin':4*h,'x':9,'y':4},

//quinta filaa... J.....





{'i':53,'pastilla':'J1','A':false,'xin':9*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':10*a,'yfin':h,'x':10,'y':1},
{'i':54,'pastilla':'J2','A':false,'xin':9*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':10*a,'yfin':2*h,'x':10,'y':2},
{'i':55,'pastilla':'J3','A':false,'xin':9*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':10*a,'yfin':3*h,'x':10,'y':3},
{'i':56,'pastilla':'J4','A':false,'xin':9*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':10*a,'yfin':4*h,'x':10,'y':4},


//quinta filaa... .k....



{'i':57,'pastilla':'K1','A':false,'xin':10*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':11*a,'yfin':h,'x':11,'y':1},
{'i':58,'pastilla':'K2','A':false,'xin':10*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':11*a,'yfin':2*h,'x':11,'y':2},
{'i':59,'pastilla':'K3','A':false,'xin':10*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':11*a,'yfin':3*h,'x':11,'y':3},
{'i':60,'pastilla':'K4','A':false,'xin':10*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':11*a,'yfin':4*h,'x':11,'y':4},



//quinta filaa... L.....


{'i':61,'pastilla':'L1','A':false,'xin':11*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':12*a,'yfin':h,'x':12,'y':1},
{'i':62,'pastilla':'L2','A':false,'xin':11*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':12*a,'yfin':2*h,'x':12,'y':2},
{'i':63,'pastilla':'L3','A':false,'xin':11*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':12*a,'yfin':3*h,'x':12,'y':3},
{'i':64,'pastilla':'L4','A':false,'xin':11*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':12*a,'yfin':4*h,'x':12,'y':4},

//quinta filaa... M.....


{'i':65,'pastilla':'M1','A':false,'xin':12*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':13*a,'yfin':h,'x':13,'y':1},
{'i':66,'pastilla':'M2','A':false,'xin':12*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':13*a,'yfin':2*h,'x':13,'y':2},
{'i':67,'pastilla':'M3','A':false,'xin':12*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':13*a,'yfin':3*h,'x':13,'y':3},
{'i':68,'pastilla':'M4','A':false,'xin':12*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':13*a,'yfin':4*h,'x':13,'y':4},


//quinta filaa... N.....



{'i':69,'pastilla':'N1','A':false,'xin':13*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':14*a,'yfin':h,'x':14,'y':1},
{'i':70,'pastilla':'N2','A':false,'xin':13*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':14*a,'yfin':2*h,'x':14,'y':2},
{'i':71,'pastilla':'N3','A':false,'xin':13*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':14*a,'yfin':3*h,'x':14,'y':3},
{'i':72,'pastilla':'N4','A':false,'xin':13*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':14*a,'yfin':4*h,'x':14,'y':4},




//quinta filaa... NN.....




{'i':73,'pastilla':'NN1','A':false,'xin':14*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':15*a,'yfin':h,'x':15,'y':1},
{'i':76,'pastilla':'NN2','A':false,'xin':14*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':15*a,'yfin':2*h,'x':15,'y':2},
{'i':77,'pastilla':'NN3','A':false,'xin':14*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':15*a,'yfin':3*h,'x':15,'y':3},
{'i':78,'pastilla':'NN4','A':false,'xin':14*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':15*a,'yfin':4*h,'x':15,'y':4},

//quinta filaa... O.....



{'i':79,'pastilla':'O1','A':false,'xin':15*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':16*a,'yfin':h,'x':16,'y':1},
{'i':80,'pastilla':'O2','A':false,'xin':15*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':16*a,'yfin':2*h,'x':16,'y':2},
{'i':81,'pastilla':'O3','A':false,'xin':15*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':16*a,'yfin':3*h,'x':16,'y':3},
{'i':82,'pastilla':'O4','A':false,'xin':15*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':16*a,'yfin':4*h,'x':16,'y':4},






//quinta filaa... P.....


{'i':83,'pastilla':'P1','A':false,'xin':16*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':17*a,'yfin':h,'x':17,'y':1},
{'i':84,'pastilla':'P2','A':false,'xin':16*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':17*a,'yfin':2*h,'x':17,'y':2},
{'i':85,'pastilla':'P3','A':false,'xin':16*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':17*a,'yfin':3*h,'x':17,'y':3},
{'i':86,'pastilla':'P4','A':false,'xin':16*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':17*a,'yfin':4*h,'x':17,'y':4},


//quinta filaa... Q.....


{'i':87,'pastilla':'Q1','A':false,'xin':17*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':18*a,'yfin':h,'x':18,'y':1},
{'i':88,'pastilla':'Q2','A':false,'xin':17*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':18*a,'yfin':2*h,'x':18,'y':2},
{'i':89,'pastilla':'Q3','A':false,'xin':17*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':18*a,'yfin':3*h,'x':18,'y':3},
{'i':90,'pastilla':'Q4','A':false,'xin':17*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':18*a,'yfin':4*h,'x':18,'y':4},

//quinta filaa... R.....



{'i':91,'pastilla':'R1','A':false,'xin':18*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':19*a,'yfin':h,'x':19,'y':1},
{'i':92,'pastilla':'R2','A':false,'xin':18*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':19*a,'yfin':2*h,'x':19,'y':2},
{'i':93,'pastilla':'R3','A':false,'xin':18*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':19*a,'yfin':3*h,'x':19,'y':3},
{'i':94,'pastilla':'R4','A':false,'xin':18*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':19*a,'yfin':4*h,'x':19,'y':4},

//quinta filaa... S.....

{'i':95,'pastilla':'S1','A':false,'xin':19*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':20*a,'yfin':h,'x':20,'y':1},
{'i':96,'pastilla':'S2','A':false,'xin':19*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':20*a,'yfin':2*h,'x':20,'y':2},
{'i':97,'pastilla':'S3','A':false,'xin':19*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':20*a,'yfin':3*h,'x':20,'y':3},
{'i':98,'pastilla':'S4','A':false,'xin':19*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':20*a,'yfin':4*h,'x':20,'y':4},
{'i':99,'pastilla':'S5','A':false,'xin':19*a+this.columna_izquierda,'yin':5*h+this.margin_alto,'xfin':20*a,'yfin':5*h,'x':20,'y':5},
{'i':100,'pastilla':'S6','A':false,'xin':19*a+this.columna_izquierda,'yin':6*h+this.margin_alto,'xfin':20*a,'yfin':6*h,'x':20,'y':6},
{'i':101,'pastilla':'S7','A':false,'xin':19*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':20*a,'yfin':7*h,'x':20,'y':7},
{'i':102,'pastilla':'S8','A':false,'xin':19*a+this.columna_izquierda,'yin':8*h+this.margin_alto,'xfin':20*a,'yfin':8*h,'x':20,'y':8},




//quinta filaa... S.....

{'i':103,'pastilla':'T1','A':false,'xin':20*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':21*a,'yfin':h,'x':21,'y':1},
{'i':104,'pastilla':'T2','A':false,'xin':20*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':21*a,'yfin':2*h,'x':21,'y':2},
{'i':105,'pastilla':'T3','A':false,'xin':20*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':21*a,'yfin':3*h,'x':21,'y':3},
{'i':106,'pastilla':'T4','A':false,'xin':20*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':21*a,'yfin':4*h,'x':21,'y':4},
{'i':107,'pastilla':'T5','A':false,'xin':20*a+this.columna_izquierda,'yin':5*h+this.margin_alto,'xfin':21*a,'yfin':5*h,'x':21,'y':5},
{'i':108,'pastilla':'T6','A':false,'xin':20*a+this.columna_izquierda,'yin':6*h+this.margin_alto,'xfin':21*a,'yfin':6*h,'x':21,'y':6},
{'i':109,'pastilla':'T7','A':false,'xin':20*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':21*a,'yfin':7*h,'x':21,'y':7},
{'i':110,'pastilla':'T8','A':false,'xin':20*a+this.columna_izquierda,'yin':8*h+this.margin_alto,'xfin':21*a,'yfin':8*h,'x':21,'y':8},


//quinta filaa... T.....

{'i':111,'pastilla':'U1','A':false,'xin':21*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':22*a,'yfin':h,'x':22,'y':1},
{'i':112,'pastilla':'U2','A':false,'xin':21*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':22*a,'yfin':2*h,'x':22,'y':2},
{'i':113,'pastilla':'U3','A':false,'xin':21*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':22*a,'yfin':3*h,'x':22,'y':3},
{'i':114,'pastilla':'U4','A':false,'xin':21*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':22*a,'yfin':4*h,'x':22,'y':4},
{'i':115,'pastilla':'U5','A':false,'xin':21*a+this.columna_izquierda,'yin':5*h+this.margin_alto,'xfin':22*a,'yfin':5*h,'x':22,'y':5},
{'i':116,'pastilla':'U6','A':false,'xin':21*a+this.columna_izquierda,'yin':6*h+this.margin_alto,'xfin':22*a,'yfin':6*h,'x':22,'y':6},
{'i':117,'pastilla':'U7','A':false,'xin':21*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':22*a,'yfin':7*h,'x':22,'y':7},
{'i':118,'pastilla':'U8','A':false,'xin':21*a+this.columna_izquierda,'yin':8*h+this.margin_alto,'xfin':22*a,'yfin':8*h,'x':22,'y':8},
//quinta filaa... V.....


{'i':119,'pastilla':'V1','A':false,'xin':22*a+this.columna_izquierda,'yin':h+this.margin_alto,'xfin':23*a,'yfin':h,'x':23,'y':1},
{'i':120,'pastilla':'V2','A':false,'xin':22*a+this.columna_izquierda,'yin':2*h+this.margin_alto,'xfin':23*a,'yfin':2*h,'x':23,'y':2},
{'i':121,'pastilla':'V3','A':false,'xin':22*a+this.columna_izquierda,'yin':3*h+this.margin_alto,'xfin':23*a,'yfin':3*h,'x':23,'y':3},
{'i':122,'pastilla':'V4','A':false,'xin':22*a+this.columna_izquierda,'yin':4*h+this.margin_alto,'xfin':23*a,'yfin':4*h,'x':23,'y':4},
{'i':123,'pastilla':'V5','A':false,'xin':22*a+this.columna_izquierda,'yin':5*h+this.margin_alto,'xfin':23*a,'yfin':5*h,'x':23,'y':5},
{'i':124,'pastilla':'V6','A':false,'xin':22*a+this.columna_izquierda,'yin':6*h+this.margin_alto,'xfin':23*a,'yfin':6*h,'x':23,'y':6},
{'i':125,'pastilla':'V7','A':false,'xin':22*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':23*a,'yfin':7*h,'x':23,'y':7},
{'i':126,'pastilla':'V8','A':false,'xin':22*a+this.columna_izquierda,'yin':8*h+this.margin_alto,'xfin':23*a,'yfin':8*h,'x':23,'y':8},

////Tiro Raso..


{'i':127,'tiroraso':false,'pastilla':'E7','A':false,'xin':4*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':5*a,'yfin':7*h,'x':5,'y':7},
{'i':128,'tiroraso':false,'pastilla':'F7','A':false,'xin':5*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':6*a,'yfin':7*h,'x':6,'y':7},
{'i':129,'tiroraso':false,'pastilla':'G7','A':false,'xin':6*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':7*a,'yfin':7*h,'x':7,'y':7},
{'i':130,'tiroraso':false,'pastilla':'H7','A':false,'xin':7*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':8*a,'yfin':7*h,'x':8,'y':7},
{'i':131,'tiroraso':false,'pastilla':'I7','A':false,'xin':8*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':9*a,'yfin':7*h,'x':9,'y':7},
{'i':132,'tiroraso':false,'pastilla':'J7','A':false,'xin':9*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':10*a,'yfin':7*h,'x':10,'y':7},
{'i':133,'tiroraso':false,'pastilla':'K7','A':false,'xin':10*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':11*a,'yfin':7*h,'x':11,'y':7},
{'i':134,'tiroraso':false,'pastilla':'L7','A':false,'xin':11*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':12*a,'yfin':7*h,'x':12,'y':7},
{'i':135,'tiroraso':false,'pastilla':'M7','A':false,'xin':12*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':13*a,'yfin':7*h,'x':13,'y':7},
{'i':136,'tiroraso':false,'pastilla':'N7','A':false,'xin':13*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':14*a,'yfin':7*h,'x':14,'y':7},
{'i':137,'tiroraso':false,'pastilla':'NN7','A':false,'xin':14*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':15*a,'yfin':7*h,'x':15,'y':7},
{'i':138,'tiroraso':false,'pastilla':'O7','A':false,'xin':15*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':16*a,'yfin':7*h,'x':16,'y':7},
{'i':139,'tiroraso':false,'pastilla':'P7','A':false,'xin':16*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':17*a,'yfin':7*h,'x':17,'y':7},
{'i':140,'tiroraso':false,'pastilla':'Q7','A':false,'xin':17*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':18*a,'yfin':7*h,'x':18,'y':7},
{'i':141,'tiroraso':false,'pastilla':'R7','A':false,'xin':18*a+this.columna_izquierda,'yin':7*h+this.margin_alto,'xfin':19*a,'yfin':7*h,'x':19,'y':7},


    ]



    //this.storage.set('array_secuencia',this.array)

    // this.array_vacio = this.array




  }


  //Touch Screen Pastillas



  arrastra(data){

       


        var myLocation = data.touches[0];
      var realTarget = document.elementFromPoint(myLocation.clientX, myLocation.clientY);


      var filteredObj = this.array.find(function(item, i){
        if(item.pastilla === realTarget.className.split(' ')[0]){


          
          return i+1;
        }
      });


      if(filteredObj){


      filteredObj.A=true


      }


      this.lastX=data.touches[0].pageX
      this.lastY=data.touches[0].pageY



       // if (this.lastY<=420 && this.activatiro==false){

         //   this.sacaletra(this.Xi,this.Yi,this.lastX,this.lastY)

        //}

        

      

  }









    pintarandomverde(){


        this.limpia()

        let sec  = this.primer_reloj.split(':')

        this.duracion =parseInt(sec[2])

        let timerId = setInterval(() => { 

        this.duracion=this.duracion-1 

        let aleatorio =  Math.round(Math.random()*20)

        console.log(this.array_primer_arco[aleatorio])

        this.array.find(el => el.pastilla === this.array_primer_arco[aleatorio]).A=true

        }, 1000);

        setTimeout(() => { clearInterval(timerId);  }, this.duracion*1000);





    }

     pintarandomamarillo(){

      console.log('data....')


        this.limpia()

        let sec  = this.primer_reloj.split(':')

        this.duracion =parseInt(sec[2])

        let timerId = setInterval(() => { 

        this.duracion=this.duracion-1 

        let aleatorio =  Math.round(Math.random()*20)

        console.log(this.array_segundo_arco[aleatorio])

        this.array.find(el => el.pastilla === this.array_segundo_arco[aleatorio]).A=true

        
        //this.array[aleatorio].A = true




        }, 1000);

        setTimeout(() => { clearInterval(timerId);  }, this.duracion*1000);



    }

        pintarandomazul(){

      console.log('data....')

        this.limpia()

        let sec  = this.primer_reloj.split(':')

        this.duracion =parseInt(sec[2])

        let timerId = setInterval(() => { 

        this.duracion=this.duracion-1 

        let aleatorio =  Math.round(Math.random()*20)

        console.log(this.array_tercer_arco[aleatorio])

        this.array.find(el => el.pastilla === this.array_tercer_arco[aleatorio]).A=true

        
        //this.array[aleatorio].A = true




        }, 1000);

        setTimeout(() => { clearInterval(timerId);  }, this.duracion*1000);



    }


         pintarandorojo(){

     

        this.limpia()

        let sec  = this.primer_reloj.split(':')

        this.duracion =parseInt(sec[2])

        let timerId = setInterval(() => { 

        this.duracion=this.duracion-1 

        let aleatorio =  Math.round(Math.random()*20)

        console.log(this.array_cuarto_arco[aleatorio])

        this.array.find(el => el.pastilla === this.array_cuarto_arco[aleatorio]).A=true

        
        //this.array[aleatorio].A = true




        }, 1000);

        setTimeout(() => { clearInterval(timerId);  }, this.duracion*1000);



    }






    pintaarcoverde(){


      this.limpia()

          if (this.activaverde==false){

            this.array[0]['A']=true;this.array[1]['A']=true;this.array[2]['A']=true;this.array[3]['A']=true;this.array[4]['A']=true;this.array[5]['A']=true;this.array[6]['A']=true;this.array[7]['A']=true;this.array[8]['A']=true;this.array[16]['A']=true;this.array[24]['A']=true;this.array[32]['A']=true;this.array[36]['A']=true;this.array[40]['A']=true;this.array[44]['A']=true;this.array[48]['A']=true;this.array[52]['A']=true;this.array[56]['A']=true;this.array[60]['A']=true;this.array[64]['A']=true;this.array[68]['A']=true;this.array[72]['A']=true;this.array[76]['A']=true;this.array[80]['A']=true;this.array[84]['A']=true;this.array[88]['A']=true;this.array[92]['A']=true;this.array[100]['A']=true;this.array[108]['A']=true;this.array[116]['A']=true;this.array[117]['A']=true;this.array[118]['A']=true;this.array[119]['A']=true;this.array[120]['A']=true;this.array[121]['A']=true;this.array[122]['A']=true;this.array[123]['A']=true;
            this.activaverde=true


          }
          else{

            this.limpia()
            this.activaverde=false

          }

      
    }

    pintaamarillo(){


       this.limpia()

      

          if (this.activaamarillo==false){

            this.array[9]['A']=true;this.array[10]['A']=true;this.array[11]['A']=true;this.array[12]['A']=true;this.array[13]['A']=true;this.array[14]['A']=true;this.array[15]['A']=true;this.array[17]['A']=true;this.array[25]['A']=true;this.array[33]['A']=true;this.array[37]['A']=true;this.array[41]['A']=true;this.array[45]['A']=true;this.array[49]['A']=true;this.array[53]['A']=true;this.array[57]['A']=true;this.array[61]['A']=true;this.array[65]['A']=true;this.array[69]['A']=true;this.array[73]['A']=true;this.array[77]['A']=true;this.array[81]['A']=true;this.array[85]['A']=true;this.array[89]['A']=true;this.array[93]['A']=true;this.array[101]['A']=true;this.array[109]['A']=true;this.array[110]['A']=true;this.array[111]['A']=true;this.array[112]['A']=true;this.array[113]['A']=true;this.array[114]['A']=true;this.array[115]['A']=true;

            this.activaamarillo=true


          }
          else{

            this.limpia()
            this.activaamarillo=false

          }

    }
     

  


    pintaazul(){


      this.limpia()

          if (this.activaazul==false){

            this.array[18]['A']=true;this.array[19]['A']=true;this.array[20]['A']=true;this.array[21]['A']=true;this.array[22]['A']=true;this.array[23]['A']=true;this.array[26]['A']=true;this.array[34]['A']=true
            this.array[38]['A']=true;this.array[42]['A']=true;this.array[46]['A']=true;this.array[50]['A']=true;this.array[54]['A']=true;this.array[58]['A']=true;this.array[62]['A']=true;this.array[66]['A']=true;this.array[70]['A']=true;this.array[74]['A']=true;this.array[78]['A']=true;this.array[82]['A']=true;this.array[86]['A']=true;this.array[90]['A']=true;this.array[94]['A']=true;this.array[102]['A']=true;this.array[103]['A']=true;this.array[104]['A']=true;this.array[105]['A']=true;this.array[106]['A']=true;this.array[107]['A']=true;

            this.activaazul=true


          }
          else{

            this.limpia()
            this.activaazul=false

          }



    }


    pintarojo(){

      this.limpia()

          if (this.activarojo==false){

            this.array[27]['A']=true;this.array[28]['A']=true;this.array[29]['A']=true;this.array[30]['A']=true;this.array[31]['A']=true;this.array[35]['A']=true;this.array[39]['A']=true;this.array[43]['A']=true;this.array[47]['A']=true;this.array[51]['A']=true;this.array[55]['A']=true;this.array[59]['A']=true;this.array[63]['A']=true;this.array[67]['A']=true;this.array[71]['A']=true;this.array[75]['A']=true;this.array[79]['A']=true;this.array[83]['A']=true;this.array[87]['A']=true;this.array[91]['A']=true;this.array[95]['A']=true;this.array[96]['A']=true;this.array[97]['A']=true;this.array[98]['A']=true;this.array[99]['A']=true;

            this.activarojo=true


          }
          else{

            this.limpia()
            this.activarojo=false

          }
    }

    ionViewDidLoad(){

      this.clock=new Date('2018-02-01 5:05:00')

      this.primer_reloj='00:00:02'

      this.segundo_reloj='00:00:05'

      this.grabando=false

      this.enejecucion=false

      this.indexjugador=0

      this.estado_jugador=''

    
              
    }


    secuencia(){


      this.agregatoast('Funcion SECUENCIA, activa grupos de areas')

      this.limpia()

      this.limpiatiroraso()


      this.activasecuencia=true


     for (var i = 0; i < this.indexsecuencia; i++) {
          
             this.storage.remove('secuencia'+i)
          }

     
      this.activoarea=false
      this.activarandom=false
      this.activatiro=false
      this.muestraplay=false
      this.indexjugador=0

      this.jugadoresseleccionados=[]


    }

    parasecuencia(){


      this.estado_jugador=''

      this.activasecuencia=false

      this.indexsecuencia=0


      if (this.activatiro==true){


             this.limpiatiroraso()

             
             

      }

      for (var i = 0; i < this.indexsecuencia; i++) {
          
             this.storage.remove('secuencia'+i)
          }

    }



    juegoaleatorio(){


     this.dataprovider.listajuegos(this.club_id).subscribe(data=>{

       this._juegos=data

       let juegoazar = Math.round(Math.random()*this._juegos.length)

       console.log(juegoazar)


         this.navCtrl.push(HomePage, {
      play: data[juegoazar].jue_codigo,
    })





     })

      


    }


    consecutivo(){




    let profileModal = this.modalCtrl.create(ConsecutivoComponent, {});
   profileModal.onDidDismiss(data => {


     console.log(data)



   })
   profileModal.present();





    }



    randompinta(){

      // Saca pastilla aletoria

             let pastilla = Math.round(Math.random()*137)

                if (this.array[pastilla].A==false){

                  this.array[pastilla].A=true
                }
                else{

                  if (pastilla+1<138){

                      this.array[pastilla+1].A=true
                  }
                  

                }

                if(this.duracion==0){

                    this.randomevaluacion()

                }



    }

    randomevaluacion(){


                  this.estado_jugador='En Evaluacion'

                  this.duracion=this.duracion_segundo_reloj

                  this.limpia()

                  //let timerId = setInterval(() => { this.duracion=this.duracion-1 }, 1000);

                  //setTimeout(() => { clearInterval(timerId);  }, this.duracion*1000);

    }




    random(){

    



       if(this.jugadoresseleccionados.length==this.indexjugador){

              this.jugadoresseleccionados=[]

              this.activapelota=false

              this.estado_jugador='Preparacion'

              this.activaplay=false

              this.activarandom=false

              this.indexjugador=0

              
       }

  

          this.agregatoast('Funcion RANDOM, selecciona sectores aleatoriamente por el sistema')

          this.limpia()

          this.limpiatiroraso()


          let sec  = this.primer_reloj.split(':')

          this.duracion =1


          this.secuencia_random = new Array(this.duracion)


          this.randompinta();


          console.log('secuencia random',this.secuencia_random)

          //this.duracion =200

          /*let timerId = setInterval(() => {
        
          this.randompinta();
          this.duracion=this.duracion-1

          }, 1000);*/

          

          this.activarandom=true
          this.activoarea=false
          this.activaintermitente=false
          this.activasecuencia=false
          this.grabando=false
          this.activatiro=false


          //this.armajugada()
          



     


    }

    intermitente(){

      this.activaintermitente=true
      this.activoarea=false
      this.activasecuencia=false
      this.activarandom=false
      this.activatiro=false
  
    }


    paraintermitente(){

      this.activaintermitente=false
    }





    pararandom(){

      this.activarandom=false
      this.limpia()
    }

    tiro(){


      this.agregatoast('Funcion TIRO RASO, selecciona sola la area superior de verde')
      this.activatiro=true
      this.activoarea=false
      
      this.activasecuencia=false
      this.activarandom=false
      this.limpia()
      this.limpiatiroraso()

     


    }

    paratiro(){

      this.activatiro=false

      this.limpia()

      this.limpiatiroraso()

      console.log('pppp',this.activatiro)



    }


    retrocede(){

       if (this.activaplay==true || this.activarandom==true){

          this.duracion=this.duracion-1
     
        }
      
    }


    preload() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });

    this.loading.present();

  
  }



    


     inicia_segundo_reloj(){



       this.limpia()

       this.limpiatiroraso()

       this.enejecucion=true

       this.estado_jugador='En Evaluacion'

       this.agregatoast('Indicanos en el tablero donde pateo el jugador !!')

       this.duracion = this.duracion_segundo_reloj

       if (this.activaplay==true){

            let timerid = setInterval(() => this.retrocede(), 1000);

            setTimeout(() => { clearInterval(timerid);}, this.duracion*1000);

       }

         


    }



    sacain(data){

        return data.sort((a, b) => (a.xin > b.xin) ? 1 : -1)[0]
    }


    sacafin(data){

        return data.sort((a, b) => (a.xfin < b.xfin) ? 1 : -1)[0]
    }


    ordenapastillas(data){

          this.xin = data.sort((a, b) => (a.xin > b.xin) ? 1 : -1)[0].xin

          this.xfin = data.sort((a, b) => (a.xfin < b.xfin) ? 1 : -1)[0].xfin

          this.yin = data.sort((a, b) => (a.yin > b.yin) ? 1 : -1)[0].yin

          this.yfin = data.sort((a, b) => (a.yfin < b.yfin) ? 1 : -1)[0].yfin

    }

   

   

    play(){


      if(this.estado_jugador==''){

              ///Saca Valores

            
              this.fp =this.array.filter(word => word.A == true)

            
              if(this.fp.length>0){

                 
                      this.ordenapastillas(this.fp)

              }
             

      }



      // Si no selecciona ningun jugador

      if (this.jugadoresseleccionados.length==0){


                if(this.tiposdetiroseleccionados && this.fp.length>0){


                              // Comienza el juego.....

                              this.estado_jugador='Preparacion'


                              // Primer reloj

                              let sec  = this.primer_reloj.split(':')

                              this.duracion =parseInt(sec[2])

                              this.duracion_primer_reloj=parseInt(sec[2])

                              //Segundo reloj

                              let sec2  = this.segundo_reloj.split(':')

                              this.duracion_segundo_reloj = parseInt(sec2[2])


                              let timerid = setInterval(() => this.retrocede(), 1000);

                              setTimeout(() => { clearInterval(timerid);

                              this.inicia_segundo_reloj();

                              }, this.duracion*1100);

                              this.activaplay=true


                             if (this.activasecuencia==true){


                                  //(2) Si es secuencia al poner invertir y poner play

                                  this.ordernsecuencia = this.listasecuencia[this.indexsecuenciacalificacion]+1

                                  console.log('Secuencia Valor: ',this.listasecuencia[this.indexsecuenciacalificacion])

                                  this.storage.get('secuencia'+this.listasecuencia[this.indexsecuenciacalificacion]).then((val)=>{

                                        this.array=val

                                        this.indexsecuenciacalificacion = this.indexsecuenciacalificacion + 1

                                  })



                                  this.agregatoast('Se inicio la secuencia ' +this.indexsecuenciacalificacion  )

                            }
                            else{


                                    this.agregatoast('Comencemos a jugar. Preparacion del jugador')


                            }



                }
                else{

                  this.agregatoast('Seleccione Tipo de Tiro o Seleccione una Area')

                }

  


      }

      //Pone Play con jugadores

      else{


                this.storage.set('jugadoresseleccionados',this.jugadoresseleccionados)

                this.storage.set('tiposdetiroseleccionados',this.tiposdetiroseleccionados)

                this.storage.set('array',this.array)

                this.storage.set('primer_reloj',this.primer_reloj)

                this.storage.set('segundo_reloj',this.segundo_reloj)

                // Comienza el juego.....

                this.estado_jugador='Preparacion'

                // Primer reloj

                let sec  = this.primer_reloj.split(':')

                this.duracion =parseInt(sec[2])

                this.duracion_primer_reloj=parseInt(sec[2])

                //Segundo reloj

                let sec2  = this.segundo_reloj.split(':')

                this.duracion_segundo_reloj = parseInt(sec2[2])


                let timerid = setInterval(() => this.retrocede(), 1000);

                setTimeout(() => { clearInterval(timerid);

                this.inicia_segundo_reloj();

                }, this.duracion*1100);

                this.activaplay=true

                if (this.activasecuencia==true){


                     this.jugador_actual=this.jugadoresseleccionados[this.indexjugador]



                      //(2) Si es secuencia al poner invertir y poner play

                      console.log('this.listasecuencia',this.listasecuencia)

                      this.ordernsecuencia = this.listasecuencia[this.indexsecuenciacalificacion]+1


                      console.log('Secuencia Valor: ',this.listasecuencia[this.indexsecuenciacalificacion])

                      this.storage.get('secuencia'+this.listasecuencia[this.indexsecuenciacalificacion]).then((val)=>{

                      this.array=val

                      this.indexsecuenciacalificacion = this.indexsecuenciacalificacion + 1

                      })

                      this.agregatoast('Se inicio la secuencia ' +this.indexsecuenciacalificacion)


                     


              }

              // Si no es secuencia

              else{


                      this.jugador_actual=this.jugadoresseleccionados[this.indexjugador]

                      this.indexjugador=this.indexjugador+1

                      this.agregatoast('Comencemos a jugar. Preparacion del jugador')


              }




      }

          


    }






    

    paraplay(){

      this.activaplay=false

      this.agregatoast('Se detuvo el juego...')
    }








     ///Arrastre Touch


    sacaletra(xin,yin,xfin,yfin){


      this.xin=xin
      this.yin=yin
      this.xfin=xfin
      this.yfin=yfin





      console.log('Puntos...',xin,yin,xfin,yfin)


      this.limpia()

      

      this.coordenadas =parseInt(xin)+'-'+parseInt(yin)

      this.coordenadas2 =parseInt(xfin)+'-'+parseInt(yfin)


      if ((xfin >= xin) && (yfin>=yin)){


           for (var i = 0; i < 124; i++) {


                 if (yfin >= this.array[i]['yfin'] && yin <= this.array[i]['yin'] && xfin >= this.array[i]['xfin'] && xin <= this.array[i]['xin'] ){

             
                     this.array[i]['A']=true

                     


                 }
            }

      }


      if((xfin <=xin) && (yfin>=yin)){


          for (var i = 0; i < 124; i++) {


                 if (yfin >= this.array[i]['yfin'] && yin <= this.array[i]['yin'] && xfin <= this.array[i]['xfin'] && xin >= this.array[i]['xin'] ){

             
                     this.array[i]['A']=true


                 }
            }



      }


      if((xfin <=xin) && (yfin<=yin)){


          for (var i = 0; i < 124; i++) {


                 if (yfin <= this.array[i]['yfin'] && yin >= this.array[i]['yin'] && xfin <= this.array[i]['xfin'] && xin >= this.array[i]['xin'] ){

             
                     this.array[i]['A']=true


                 }
            }



      }

       if((xfin >=xin) && (yfin<=yin)){


          for (var i = 0; i < 124; i++) {


                 if (yfin <= this.array[i]['yfin'] && yin >= this.array[i]['yin'] && xfin >= this.array[i]['xfin'] && xin <= this.array[i]['xin'] ){

             
                     this.array[i]['A']=true


                 }
            }



      }





 


    }


     agregatoast(data) {
    let toast = this.toastCtrl.create({
      message: data,
      duration: 2000,
      cssClass: 'mitoast',
      position:'bottom',
    });
    toast.present();
  }


     agregatoastcentro(data) {
    let toast = this.toastCtrl.create({
      message: data,
      duration: 2000,
      cssClass: 'mitoast',
      position:'top',
    });
    toast.present();
  }



   selecciona(event) {


      console.log('selecciona..',event)


      this.areaevaluada =event

      if (this.activatiro==true){


                  if (this.array_tiro_raso.indexOf(event.pastilla)!=-1){


                          if (event.tiroraso==true){

                              event.A=false

                              event.tiroraso=false

                          }
                          else{

                                event.A=false

                                event.tiroraso=true

                          }



                  }


      }

      //Pinta  o despinta pastilla seleccionada
      else{


                          if (event.A==true){

                            event.A=false

                          }
                          else{

                            event.A=true

                          }


      }

      


     if (this.estado_jugador=='En Evaluacion'){


         if (this.activasecuencia==true){


                 ///Si Califica 

                this.duracion=this.duracion_primer_reloj

                
                ///(3) Al evaluar y es invertir en Secuencia

                  console.log('ancho',this.listasecuencia.length,this.indexsecuencia)

                  if (this.listasecuencia.length == this.indexsecuenciacalificacion){


                      console.log('Entre...... lista this.listasecuencia.length == this.indexsecuenciacalificacion')

                      this.muestracalificacionsecuencia()

                      

                      

                      this.indexsecuenciacalificacion=0

                      this.indexjugador=this.indexjugador+1

                      this.jugador_actual=this.jugadoresseleccionados[this.indexjugador]


                  }
                  else{

                        this.play()

                  }


         }

         /// En Evaluacion

         else{

                 // Evaluacion en Random

                 if(this.activarandom==true && this.estado_jugador=='En Evaluacion'){

                   

                   
                     //Calificando despues de
                     this.secuencia_random[this.indexcalificarandom]=event

                     this.indexcalificarandom++

                     

                     if (this.secuencia_random.length==this.indexcalificarandom){

                       this.muestracalificacion(4)
                     }



                 }



                 //Evaluacion normal Calculo Matematico del Puntaje


                 else{


                       /// Si es una pastilla

                       if( this.fp.length==1){


                             let x1=event.x
                             let y1=event.y

                             let x2=this.fp[0].x
                             let y2=this.fp[0].y

                             let x3 =(x1-x2)*(x1-x2)
                             let y3 =(y1-y2)*(y1-y2)


                             this.muestracalificacion(Math.sqrt(x3+y3)*0.25)



                       }
                       else{


                        // Si es una area

                          let x1=event.x
                          let y1=event.y

                          let x2= (this.xin+this.xfin)/2
                          let y2= (this.yin+this.yfin)/2

                          let x3 = (x1-x2)*(x1-x2)
                          let y3 = (y1-y2)*(y1-y2)

                          this.muestracalificacion(Math.sqrt(x3+y3)*0.25)


                       }



                       ///


                          if (event.A==true){

                            event.A=false

                          }
                          else{

                            event.A=true

                          }

                       
                            


                 }

         }

         

     }
    


    
    
  }



    muestraresultado(data){


console.log(data)


    let profileModal = this.modalCtrl.create(ResultadoComponent, {'data':data,'club':this.club_id});
   profileModal.onDidDismiss(data => {



   })

      profileModal.present();



 }



  muestracalificacionsecuencia(){





    let profileModal = this.modalCtrl.create(CalificajugadorPage, {'jugador':this.jugador_actual});
   profileModal.onDidDismiss(data => {


    console.log('this.indexjugador',this.indexjugador,this.jugadoresseleccionados.length)


    if(this.indexjugador<this.jugadoresseleccionados.length){

        this.play()

    }
    else{


      this.parasecuencia()

      this.muestraplay=false

      this.activaplay=false

      this.jugadoresseleccionados=[]
    }

         

   })
   profileModal.present();



  }



  muestrajuegos(){





    let profileModal = this.modalCtrl.create(JuegosComponent, {});
   profileModal.onDidDismiss(data => {



   })
   profileModal.present();



  }


  filtrajugadornombre(data){


      console.log('filtrajugadornombre',this.arrayjugadores)


      return this.arrayjugadores.filter(word => word.Codigo== data.split(' ')[0])


  }




  traemiperfil(){





    let profileModal = this.modalCtrl.create(MiperfilComponent, {});
   profileModal.onDidDismiss(data => {


 

   })
   profileModal.present();



  }


 

  muestracalificacion(calificacion){


       this.calificacion = calificacion

       this.duracion=this.duracion_primer_reloj

       //guardapuntaje(codigo,club,jugador,item,puntaje,area)

       let am  =this.array.filter(word => word.A == true)

       console.log('am',am)

       let pastilla = 'No definido'


       if(am.length>0){

          let pastilla = am[0].pastilla

       }

       console.log('puntaje ITEM...',this.item_extras[this.indice_item_extras])

       

      let item_id=this.item_extras[this.indice_item_extras]?this.item_extras[this.indice_item_extras].id:1



       this.dataprovider.guardapuntaje(this.juegoguardado,this.club_id,this.jugador_actual,item_id,this.calificacion,pastilla)

       /// Aumenta en 1 el repetir si rrecorre todos los jugadores
       
       if(this.jugadoresseleccionados.length==this.indexjugador && this.jugadoresseleccionados.length!=0){

             console.log('Incrementando en 1...')

              this.indexjugador=0

              this.index_numerorepetir++              
       }

       /// Si termina recorrer en repetir


       console.log('Numero Repetir ' +this.numerorepetir+'Index Numero Repetir: '+this.index_numerorepetir)


      if(this.index_numerorepetir==this.numerorepetir){


            console.log('Entre aqui....')

            this.activaplay=false

            this.estado_jugador=''

            //this.parararea()

            this.limpia()

            this.jugadoresseleccionados=[]

            this.index_numerorepetir=0

            this.indice_item_extras++


            console.log('EXTRA ITEM...?',this.item_extras,this.indice_item_extras)


            this.seteaitemextra(this.item_extras[this.indice_item_extras])


      }

      else{

              this.estado_jugador='En Preparacion'

              this.storage.get('array').then((val)=>{this.array=val})

              this.play()


              this.dataprovider.listajuegos(this.club_id).subscribe(data=>{


                         this.juegos=data

                         this.ultimojuego =  data[this.juegos.length-1]

                        
        

              })



               

              


      }




 



  }




  closeModal(){


   
    this.view.dismiss()


}



  limpia(){


     // this.jugadoresseleccionados=[]

      //this.tiposdetiroseleccionados=[]
      this.muestraplay=false


      for (var i = 0; i < 139; i++) {



          this.array[i]['A']=false       

        
           
      }
  }


  






 limpiatiroraso(){


     console.log(this.array[36].tiroraso)

     this.array[36].tiroraso=false
     this.array[40].tiroraso=false
     this.array[44].tiroraso=false
     this.array[48].tiroraso=false
     this.array[52].tiroraso=false
     this.array[56].tiroraso=false
     this.array[60].tiroraso=false
     this.array[64].tiroraso=false
     this.array[68].tiroraso=false
     this.array[72].tiroraso=false
     this.array[76].tiroraso=false
     this.array[80].tiroraso=false
     this.array[84].tiroraso=false



 }



 tableropuntajes(data){


  console.log('hshsh')
  let profileModal = this.modalCtrl.create(PuntajesComponent, {'club':this.club_id,'tipo':data});
            
            profileModal.onDidDismiss(data => {




            })

            profileModal.present();

 }

  inicia(data){


   console.log('inicia....',data.target.innerHTML)



   var filteredObj = this.array.find(function(item, i){
    if(item.pastilla === data.target.innerHTML){
      
      return i;
    }
  });

  if(filteredObj){


  filteredObj.A=true


  }


      this.Xi = data.touches[0].pageX;
      this.Yi = data.touches[0].pageY;


  }


  area(){


    this.limpia()

    this.limpiatiroraso()

    this.agregatoast('Funcion AREA, seleccione una area en el arco')

    this.activoarea=true
    this.activasecuencia=false
    this.activarandom=false
    this.activatiro=false


    this.muestraplay=false

    this.jugadoresseleccionados=[]

  }
parararea(){

this.activoarea=false

this.limpia()

}



repiteultimojuego(){

    this.storage.get('jugadoresseleccionados').then((val) => {

      this.jugadoresseleccionados=val

    })

     this.storage.get('tiposdetiroseleccionados').then((val) => {

      this.tiposdetiroseleccionados=val

    })

      this.storage.get('array').then((val) => {

      this.array=val

    })

      this.storage.get('primer_reloj').then((val) => {

      this.primer_reloj=val

    })

      this.storage.get('segundo_reloj').then((val) => {

      this.segundo_reloj = val

    })

      this.indexjugador=0

      this.muestraplay=true




}


guardadetallejuego(data){


        console.log('tiposecuencia..',this.tiposecuencia)

        this.sacatipofuncion()


      var result = {

        'area':JSON.stringify(this.array),
        'club':this.club_id,
        'data':data,
        'descripcion':this.nombre_juego,
        'indexsecuencia':this.indexsecuencia,
        'tiposecuencia':this.tiposecuencia,
        'tipofuncion':this.tipofuncion,
        'juego':this.id_juego

       
      }


      console.log('guardando....',result)

    this.http.post('http://xiencias.com:9000/guardajuego/',result)
    .subscribe(
      data=>{




      })



}

sacatipofuncion(){

      if (this.activoarea==true){ this.tipofuncion = 'Area'}
      if (this.activarandom==true){ this.tipofuncion = 'Random'}
      if (this.activatiro==true){ this.tipofuncion = 'Tiro Raso'}
      if (this.activasecuencia==true){ this.tipofuncion = 'Secuencia'}


}












  finaliza(event){


   


    if (this.estado_jugador==''){



             if (this.activatiro==true){

                        if (this.array_tiro_raso.indexOf(event.pastilla)!=-1){


                                console.log('entre....',event)


                                if (event.tiroraso==false){

                                    event.A=true

                                    event.tiroraso=true

                                }
                                else{

                                      event.A=false

                                      event.tiroraso=false

                                }



                        }


            }
            else{


                if(event){

                    this.fp =this.array.filter(word => word.A == true)

                   



                     

                }




            }








          this.storage.set('array',this.array)



          if(this.activasecuencia==true){

            console.log('this.activasecuencia',this.indexsecuencia)


            if (this.indexsecuencia>=1){

              this.agregasecuencia()

            }

          }



          if(!this.activasecuencia || this.indexsecuencia==0){


            let profileModal = this.modalCtrl.create(SeleccionajugadorComponent, {'activasecuencia':this.activasecuencia,'array':this.array,'club':this.club_id});
            
            profileModal.onDidDismiss(data => {



              console.log('item extras',data)




              this.item_extras = data





              //Guaradadetallejuego......

              this.guardadetallejuego(data)

              if (data){


                      if(data[0]){

                       console.log('Entre cerrando modal...',data)

                       this.jugadoresseleccionados = data[0].jugadores

                       this.numerorepetir = data[0].numeroveces

                       this.tiposdetiroseleccionados =data[0].tipotiro

                       this.primer_reloj = data[0].primer_reloj

                       this.segundo_reloj = data[0].segundo_reloj

                       this.dataprovider.listatiros().subscribe(data=>{

                          this._tiros=data

                          this.codigotiroseleccionado = this._tiros.filter(data=>data.Descripcion==this.tiposdetiroseleccionados)

                          console.log('Entre a guardar item')

                          

                       })

                   

                      }



                      

                      if(this.activasecuencia==true){


                          console.log(data)

                          if(data.estadosecuencia=='agregasecuencia'){


                             this.secuencianext(this.array)



                          }

                          if(data.estadosecuencia=='finalizasecuencia'){


                            this.finalizasecuencia()


                          }




                         
                      
                      }
                      else{


                          this.muestraplay=true

                      }



              }
               




          })
          profileModal.present();

          }

    }



  }









  tablero(){

     let profileModal = this.modalCtrl.create(TableroComponent, {'club':this.club_id,'jugadas':this.item_extras});
          profileModal.onDidDismiss(data => {

             console.log(data)


          })
          profileModal.present();
  }



  


   
  







  ///Setea parametros del item 




  seteaitem(item){


        this.muestraplay=true


        if(item){


                this.jugadoresseleccionados=[]

                this.primer_reloj=item.jde_tinicio

                this.segundo_reloj=item.jde_ffin

                this.array = JSON.parse(item.jde_area)

                this.tiposdetiroseleccionados = item.jde_tipo_tiro

                this.tiposdetiroseleccionadonombre = item.tipotiro

                this.numerorepetir = item.jde_repite


                console.log('Nro REPETIR....',this.numerorepetir)

             
                ///Saca Datos del Item

                this.obtienedatositem(item.jde_item).subscribe(data=>{


                    this.listajugadores = data

                                  for(let d in this.listajugadores){

                                    this.jugadoresseleccionados.push(this.listajugadores[d].jug_codigo+' '+this.listajugadores[d].jug_nombres)

                                  }

                    

                })


        }



  }


seteaitemextra(item){

                                console.log('Seteaitemextra......',item)

                                console.log('fin...')


                                if(item){


                                        this.jugadoresseleccionados=item.jugadores

                                        console.log('jugadores seleccionados..',item.jugadores)

                                        this.primer_reloj=item.primer_reloj

                                        this.segundo_reloj=item.segundo_reloj

                                        this.tiposdetiroseleccionados = item.tipotiro



                                     
                                }
                                else{

                                  this.indice_item_extras=0
                                }



}



  secuencianext(data) {

     this.preload()

     this.storage.set('secuencia'+this.indexsecuencia,this.array)

     console.log('indexsec',this.indexsecuencia)

     this.indexsecuencia=this.indexsecuencia+1

     this.agregatoastcentro('Se guardo la secuencia '+this.indexsecuencia)

     setTimeout(() => { this.limpia()  

       this.loading.dismiss()

     }, 1000);
      
  }


  preparasecuencia(juego){

    this.dataprovider.items(juego,this.club_id).subscribe(data=>{


        console.log(data)



    })



  }




 seleccionanuevojuego() {

    const prompt = this.alertCtrl.create({
      title: 'Nuevo Juego',
      message: "Ingrese el nombre del juego",
      inputs: [
        {
          name: 'valor',
          type:'texto'
        },
      ],
      buttons: [
        {
          text: 'Cerrar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Guardar',
          handler: data => {

            console.log(data)


            this.nombre_juego=data

            var result={

              'descripcion': data.valor
            }

            this.http.post('http://xiencias.com:9000/nuevojuego/',result)
            .subscribe(
            data=>{

                  this.id_juego=data
            })
          
    

            

          }
        }
      ]
    });
    prompt.present();
  }


agregasecuencia() {

    let alert = this.alertCtrl.create();
    alert.setTitle('Secuencia');

    alert.addInput({
      type: 'radio',
      label: 'Agrega Secuencia',
      value: 'agrega',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Finaliza Secuencia',
      value: 'finaliza',
      checked: false
    });




    alert.addButton('Cancel');
    alert.addButton({
      text: 'Aceptar',
      handler: data => {

         if (data=='agrega'){


            this.secuencianext(this.array)

            console.log('extras..........................',this.item_extras)

            this.guardadetallejuego(this.item_extras)
         }


         if(data=='finaliza'){


           this.secuencianext(this.array)

           this.finalizasecuencia()
         }

      }
    });
    alert.present();
  }


finalizasecuencia() {

    let alert = this.alertCtrl.create();
    alert.setTitle('Secuencia');

    alert.addInput({
      type: 'radio',
      label: 'Invertir direccion',
      value: 'invertir',
      checked: false
    });

    alert.addInput({
      type: 'radio',
      label: 'Secuencia en retroceso ',
      value: 'idavuelta',
      checked: false
    });




    alert.addButton('Cancel');
    alert.addButton({
      text: 'Aceptar',
      handler: data => {

         
          ///(1) Obtiene el tipo de secuencia

          this.tiposecuencia =data

          console.log('secuencia........',this.tiposecuencia)

          let _secnormal = new Array(this.indexsecuencia)

          let _secinverso = new Array(this.indexsecuencia)

          let _secidavuelta = new Array(2*this.indexsecuencia)
          

         for (var i = 0; i < this.indexsecuencia; i++) {

                 _secnormal[i]=i
            
                 _secinverso[i]=this.indexsecuencia-i-1

                 if(this.tiposecuencia==undefined){

                     this.listasecuencia = _secnormal


                 }

                 if(this.tiposecuencia=='invertir'){

                     this.listasecuencia = _secinverso

                 }

                 
          
          }

            

           if (this.tiposecuencia=='idavuelta'){


               _secnormal.pop()

               this.listasecuencia = _secnormal.concat(_secinverso)


           }


           console.log('secuencia..',this.listasecuencia)

           this.dataprovider.guardatiposecuencia(this.id_juego,this.tiposecuencia).subscribe(data=>{})


           this.muestraplay =true




   


           this.dataprovider.listatiros().subscribe(data=>{

                          
                          this.codigotiroseleccionado=data

                          this.codigotiroseleccionado = this.codigotiroseleccionado.filter(data=>data.Descripcion==this.tiposdetiroseleccionados)

                          console.log('Secuencia hehe',this.tiposecuencia)

                         

                       })




      }
    });
    alert.present();
  }

paramemoria(){

  this.grabando=false


}

memoria(){

this.agregatoast('Funcion MEMORIA, guarda una area en memoria puede usarlo combinado con los otros botones')


this.grabando=true


}


armalistasecuencia(){



          let _secnormal = new Array(this.indexsecuencia)

          let _secinverso = new Array(this.indexsecuencia)

          let _secidavuelta = new Array(2*this.indexsecuencia)
          

         for (var i = 0; i < this.indexsecuencia; i++) {

                 _secnormal[i]=i
            
                 _secinverso[i]=this.indexsecuencia-i-1

                 if(this.tiposecuencia==undefined){

                     this.listasecuencia = _secnormal


                 }

                 if(this.tiposecuencia=='invertir'){

                     this.listasecuencia = _secinverso

                 }

                 
          
          }

            

           if (this.tiposecuencia=='idavuelta'){


               _secnormal.pop()

               this.listasecuencia = _secnormal.concat(_secinverso)


           }





           console.log('Armando lista secuencia..',this.listasecuencia)
}




pintacuadrados(data){


       if (data.length>0){

                for (var i = 0; i < 124; i++) {

                  if (data[i]['A']==true) {

                    this.array[i]['A']=true

                  }      

                }

       }

     

}


obtienedatositem(codigo){


  return this.dataprovider.listajuegodetallejugador(this.juegoguardado,this.club_id,codigo)


}





}
