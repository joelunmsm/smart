import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http,RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { DataProvider } from '../../providers/data/data';


@IonicPage()
@Component({
  selector: 'page-nuevojugador',
  templateUrl: 'nuevojugador.html',
})
export class NuevojugadorPage {


	jugador:any={}

	club_id:any;
  sexos:any;
  tiposdedoc:any;



  constructor(public dataprovider:DataProvider,private http: HttpClient,public view: ViewController,public storage:Storage,public navCtrl: NavController, public navParams: NavParams) {

  	 

     this.sexos = dataprovider.listasexos()

     this.tiposdedoc = dataprovider.listatipodedocumento()


     this.storage.get('usuario').then((val) => {

         if(val){

             this.club_id=val.club

         }
 			
    
        })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NuevojugadorPage');
  }

   closeModal(){

    this.view.dismiss()
}


  agregajugador(data){



  	data.addproceso=1
    data.addclub=this.club_id
    data.addempresa=1


  	this.http.post('http://comunica7.com/apirestsmartgoal/public/add/jugadores',data)
    .subscribe(
      data => {

          this.closeModal()
          

        },

      error =>{


        
       
      })


  }

}
