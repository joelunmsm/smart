import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Http,RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the AgregadetallejuegoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agregadetallejuego',
  templateUrl: 'agregadetallejuego.html',
})
export class AgregadetallejuegoPage {


	nuevodetalle:any={}
  juego:any;

  club_id:any;

  constructor(public storage:Storage,private http: HttpClient,public navCtrl: NavController, public navParams: NavParams,public view: ViewController) {
  
       this.juego= navParams.get("juego")


       console.log('Juego..',this.juego)


     this.storage.get('usuario').then((val) => {

         if(val){

             this.club_id=val.club

            

         }
       
    
        })


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgregadetallejuegoPage');
  }
  //usp_add_juego_det '/add/juegodet' :: addjuego,addclub,addempresa,addarea,addtiro,addhinicio,addfin,addusuario


  agregadetallejuego(data){

  		data.addclub=this.club_id
  		data.addjuego=this.juego.Codigo
		data.addempresa=1
		data.addusuario=1
		data.addproceso=1
		data.addarea='A2'
		data.addtiro='1'
		data.addhinicio='05:00'
		data.addfin='05:00'





		 this.http.post('http://comunica7.com/apirestsmartgoal/public/add/juegodet',data)
    .subscribe(
      data => {

          console.log('ooo',data)


          this.closeModal()

   
          //this.clubes =data


        },

      error =>{



      })



  }


  closeModal(){


  	this.view.dismiss()
  }



}
