import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Storage } from '@ionic/storage';
import {IonicStorageModule} from '@ionic/storage';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { JuegosComponent } from '../components/juegos/juegos';
import { UsuariosComponent } from '../components/usuarios/usuarios';
import { SeleccionajugadorComponent } from '../components/seleccionajugador/seleccionajugador';
import { TableroComponent } from '../components/tablero/tablero';
import { JugadoresComponent } from '../components/jugadores/jugadores';
import { CalificacionesComponent } from '../components/calificaciones/calificaciones';
import { ResultadoComponent } from '../components/resultado/resultado';
import { ConsecutivoComponent } from '../components/consecutivo/consecutivo';
import { ConfiguracionComponent } from '../components/configuracion/configuracion';
import { PuntajesComponent } from '../components/puntajes/puntajes';
import { MiperfilComponent } from '../components/miperfil/miperfil';
import { ConfiguracionPage } from '../pages/configuracion/configuracion';
import { DetallejuegoPage } from '../pages/detallejuego/detallejuego';
import { FiltroPage } from '../pages/filtro/filtro';
import { EditarjugadorPage } from '../pages/editarjugador/editarjugador';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { CalificajugadorPage } from '../pages/calificajugador/calificajugador';
import { IngresarPage } from '../pages/ingresar/ingresar';
import { NuevojuegoPage } from '../pages/nuevojuego/nuevojuego';
import { NuevojugadorPage } from '../pages/nuevojugador/nuevojugador';
import { AgregadetallejuegoPage } from '../pages/agregadetallejuego/agregadetallejuego';
import { HttpClientModule,HttpClient } from '@angular/common/http'; 
import { Http, RequestOptions, HttpModule } from '@angular/http';
import { DataProvider } from '../providers/data/data';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    JuegosComponent,
    UsuariosComponent,
    JugadoresComponent,
    CalificacionesComponent,
    ConfiguracionComponent,
    SeleccionajugadorComponent,
    MiperfilComponent,
    PuntajesComponent,
    ConsecutivoComponent,
    TableroComponent,
    ResultadoComponent,
    FiltroPage,
    CalificajugadorPage,
    ConfiguracionPage,
    NuevojuegoPage,
    IngresarPage,
    AgregadetallejuegoPage,
    DetallejuegoPage,
    NuevojugadorPage,
    EditarjugadorPage
  ],
  imports: [
    BrowserModule,
    IonicStorageModule.forRoot({
    }),
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot({
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CalificajugadorPage,
    JuegosComponent,
    UsuariosComponent,
    ConsecutivoComponent,
    JugadoresComponent,
    CalificacionesComponent,
    TableroComponent,
    ResultadoComponent,
    ConfiguracionComponent,
    MiperfilComponent,
    PuntajesComponent,
    SeleccionajugadorComponent,
    JuegosComponent,
    FiltroPage,
    NuevojuegoPage,
    IngresarPage,
    AgregadetallejuegoPage,
    ConfiguracionPage,
    DetallejuegoPage,
    NuevojugadorPage,
    EditarjugadorPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ScreenOrientation,
    DataProvider
  ]
})
export class AppModule {}
