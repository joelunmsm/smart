import { NgModule } from '@angular/core';
import { JuegosComponent } from './juegos/juegos';
import { UsuariosComponent } from './usuarios/usuarios';
import { JugadoresComponent } from './jugadores/jugadores';

import { CalificacionesComponent } from './calificaciones/calificaciones';
import { ConfiguracionComponent } from './configuracion/configuracion';


@NgModule({
	declarations: [


    ],
	imports: [],
	exports: [



    ]
})
export class ComponentsModule {}
