import { Component } from '@angular/core';

/**
 * Generated class for the CalificacionesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'calificaciones',
  templateUrl: 'calificaciones.html'
})
export class CalificacionesComponent {

  text: string;

  constructor() {
    console.log('Hello CalificacionesComponent Component');
    this.text = 'Hello World';
  }

}
