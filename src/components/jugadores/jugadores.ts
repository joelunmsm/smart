import { Component } from '@angular/core';
import { Http,RequestOptions, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { DataProvider } from '../../providers/data/data';
import { IonicPage, NavController, NavParams, ModalController,AlertController,Platform } from 'ionic-angular';
import { NuevojugadorPage } from '../../pages/nuevojugador/nuevojugador';
import { EditarjugadorPage } from '../../pages/editarjugador/editarjugador';
/**
 * Generated class for the JugadoresComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'jugadores',
  templateUrl: 'jugadores.html'
})
export class JugadoresComponent {

  text: string;
  jugadores:any;
  juegos:any;
  sexos:any;


  constructor(public modalCtrl:ModalController,public dataprovider:DataProvider, private http: HttpClient) {


  	this.jugadores = this.dataprovider.traejugadores(1,"null")

      


   


  }


  nuevojugador(){

  	   let profileModal = this.modalCtrl.create(NuevojugadorPage, {});
   profileModal.onDidDismiss(data => {
     
   		this.jugadores = this.dataprovider.traejugadores(1,"null")


   });
   profileModal.present();




  }


  editarjugador(data){

       let profileModal = this.modalCtrl.create(EditarjugadorPage, {'jugador':data});
   profileModal.onDidDismiss(data => {
     
       this.jugadores = this.dataprovider.traejugadores(1,"null")


   });
   profileModal.present();


  }





}
