import { Component } from '@angular/core';
import { DataProvider } from '../../providers/data/data';
import { IonicPage, NavController, NavParams, ModalController,ViewController,Platform } from 'ionic-angular';
/**
 * Generated class for the SeleccionajugadorComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'seleccionajugador',
  templateUrl: 'seleccionajugador.html'
})
export class SeleccionajugadorComponent {

  text: string;
  jugadores:any;
  filtro:any={};
  tipotiro:any;
  juego:any=[];
  activasecuencia:any;
  array:any;
  ordernsecuencia:any;
  club_id:any;


  constructor(public view: ViewController,public dataprovider:DataProvider,public navParams:NavParams) {


    
    this.array = navParams.get("array")

    this.club_id = navParams.get("club")

    console.log('kkk',navParams.get("activasecuencia"))



  }

  ionViewDidLoad(){


    this.activasecuencia = this.navParams.get("activasecuencia")

  	this.jugadores = this.dataprovider.traejugadores(this.club_id,"null")

  	this.tipotiro = this.dataprovider.listatiros()

  	this.filtro.primer_reloj='00:00:03'

  	this.filtro.segundo_reloj='00:00:03'

    this.filtro.numeroveces=1

  }

  agrega(data){


    console.log('agregando....')

  	this.juego.push(data)

  	this.filtro={}

  	this.filtro.primer_reloj='00:00:03'

  	this.filtro.segundo_reloj='00:00:03'
    
    this.filtro.numeroveces=1

  }

  empezar(data){


    this.juego.estadosecuencia = data

  	console.log('Empezando juego...',this.juego)

  	this.view.dismiss(this.juego)
  }

  salir(){

  	this.view.dismiss(this.juego)
  }




}
