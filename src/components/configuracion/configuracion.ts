import { Component } from '@angular/core';

/**
 * Generated class for the ConfiguracionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'configuracion',
  templateUrl: 'configuracion.html'
})
export class ConfiguracionComponent {

  text: string;

  constructor() {
    console.log('Hello ConfiguracionComponent Component');
    this.text = 'Hello World';
  }

}
