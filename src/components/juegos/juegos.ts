import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,AlertController,Platform,ViewController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Http , Response } from '@angular/http';
import { Storage } from '@ionic/storage';
import { NuevojuegoPage } from '../../pages/nuevojuego/nuevojuego';
import { HomePage } from '../../pages/home/home';
import { DetallejuegoPage } from '../../pages/detallejuego/detallejuego';
import { DataProvider } from '../../providers/data/data';


/**
/**
 * Generated class for the JuegosComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'juegos',
  templateUrl: 'juegos.html'
})
export class JuegosComponent {

  text: string;
  club_id:any=1;
  juegos:any;
  juegoeliminar:any={};

  constructor(private view:ViewController,public storage :Storage,public dataprovider: DataProvider,private http: HttpClient,public modalCtrl: ModalController,public navCtrl:NavController ) {
    console.log('Hello JuegosComponent Component');
    this.text = 'Hello World';


    this.storage.get('usuario').then((val) => {

         if(val){

             this.club_id=val.club


             this.juegos=dataprovider.listajuegos(this.club_id)

         }
       
    
        })

   

     



  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad DetallejuegoPage');

  
  
        
  }
    closeModal(){

    this.view.dismiss()

  }



  agregajuego(){

  	   let profileModal = this.modalCtrl.create(NuevojuegoPage, {});
   profileModal.onDidDismiss(data => {
     
   		this.juegos=this.dataprovider.listajuegos(this.club_id)

   });
   profileModal.present();




  }

  detalle(result){


      let profileModal = this.modalCtrl.create(DetallejuegoPage, {juego: result});
   profileModal.onDidDismiss(data => {
     
      

   });
   profileModal.present();

  }


  play(data){


    console.log(data)

   
    this.navCtrl.push(HomePage, {
      play: data.jue_codigo,
    })




  }

  eliminar(data){

    this.juegoeliminar.dproceso= 3
    this.juegoeliminar.dusuario= 1
    this.juegoeliminar.ddescripcion = 1


    this.http.get('http://xiencias.com:9000/eliminajuego/'+this.club_id+'/'+data.jue_codigo).subscribe(data=>{

      this.juegos=this.dataprovider.listajuegos(this.club_id)

    })


    //this.http.put('http://comunica7.com/apirestsmartgoal/public/delete/juego/'+data.jue_codigo+'/'+this.club_id+'/1',this.juegoeliminar).subscribe(data=>{


      //  this.juegos=this.dataprovider.listajuegos(this.club_id)

    //})


    
  }







}
