import { Component } from '@angular/core';

/**
 * Generated class for the UsuariosComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'usuarios',
  templateUrl: 'usuarios.html'
})
export class UsuariosComponent {

  text: string;

  constructor() {
    console.log('Hello UsuariosComponent Component');
    this.text = 'Hello World';
  }

}
